#!/bin/bash
# Simon.B

# version alpha2 => NE PAS UTILISER CE SCRIPT, IL EST EN COURS DE DEV !
# Précision : ce script n'est conçu que pour la dernière version stable des distributions donc :
# Ubuntu en 16.04, Linux Mint en 18, Debian en 8 !

#================
# Description
#================
# Script de post-installation ultime pour de nombreuse distribution GNU/Linux de famille différente avec pleins de réglage possible

#############################################
# Run using sudo, of course.
#############################################
if [ "$UID" -ne "0" ]
then
  echo "Il faut etre root pour executer ce script. ==> sudo "
  exit 
fi 

. /etc/lsb-release

# Q1 : profil d'install
echo "#######################################"
echo "Veuillez choisir le profil de postinstallation parmi la liste ci-dessous"
echo "======================================="
echo "0 = Etab Scolaire : Ubuntu, Xubuntu, UbuMate..., Mint (xenial)"
echo "1 = Etab Scolaire : Debian (jessie)"
echo "2 = Technicien Rectorat : Ubuntu, Xubuntu, UbuMate..., Mint (xenial)"
echo "3 = Technicien Rectorat : Debian (jessie)"
echo "4 = Familial : Ubuntu, Xubuntu, UbuMate..., Mint (xenial)"
echo "5 = Perso1 : Ubuntu, Xubuntu, UbuMate..., Mint (xenial)"
echo "6 = Perso2 : Debian (jessie)"
echo "7 = Perso3 : ArchLinux"
echo "8 = TonProfil1"
echo "9 = TonProfil2"
echo "======================================="
read -p "Répondre par le chiffre correspondant (0 a 9) : " rep_profil

# Q2 : complexité du niveau d'install
echo "#######################################"
echo "Type d'installation postinstall ?"
echo "======================================="
echo "0 = Simple (aucune question posé) [choix par défaut]"
echo "1 = Avancé (plusieurs questions techniques posés)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (0 ou 1) : " rep_avance

##################################################
# Si installation avancé alors :

if [ "$rep_avance" = "1" ] ; then

# Q3 : backporter LibreOffice (ubuntu/debian)
echo "#######################################"
echo "Faut t'il backporter LibreOffice ? (uniquement pour les distribs basés sur Ubuntu ou Debian)"
echo "======================================="
echo "0 = Non, conserver la version dans les dépots de la distrib [choix par défaut]"
echo "1 = Oui, obtenir la dernière version stable possible"
echo "(nb : pour Arch, le choix n'a aucune importance puisque c'est une Rolling donc déjà la dernière version de base)"
echo "======================================="
read -p "Répondre par le chiffre correspondant (0 ou 1) : " rep_backportlo

# Q4 : extinction de la machine le soir ?
echo "#######################################"
echo "Voulez vous activer l'extinction automatique des postes le soir ?"
echo "======================================="
echo "0 = non, pas d'extinction automatique le soir (ou déjà fait dans script integrdom) [choix par défaut]"
echo "1 = oui, extinction a 19H00" 
echo "2 = oui, extinction a 21H00"
echo "3 = oui, extinction a 23H00"
echo "4 = oui, extinction a 1H du matin"
echo "======================================="
read -p "Répondre par le chiffre correspondant (0 a 4) : " rep_proghalt

# Q5 : env graphique
echo "#######################################"
echo "Souhaitez vous installer une environnement de bureau (si vous partez d'un système minimal) ?"
echo "(NB : cette question concerne uniquement les utilisateurs de Debian et Arch)"
echo "======================================="
echo "0 = non j'ai déja un environnement de bureau OU je ne suis pas sous Debian/Arch [choix par défaut]"
echo "1 = Xfce + LightDM + Extra"
echo "2 = Mate + LightDM + Extra"
echo "3 = Gnome Shell/Gnome 3 + GDM + Extra"
echo "======================================="
read -p "Répondre par le chiffre correspondant (0 a 3) : " rep_envbur

# Q6 : customisation ? ## A FAIRE ! FONCTION NON UTILISE ACTUELLEMENT !
echo "#######################################"
echo "Activer la customisation graphique avec un profil défini (skel) ?"
echo "======================================="
echo "0 = non, conserver un bureau standard [choix par défaut]"
echo "1 = oui activer la customisation graphique si c'est possible..."
echo "======================================="
read -p "Répondre par le chiffre correspondant (0 a 3) : " rep_custom

fi 
# Fin question menu avancé
##################################################

# Programmation extinction (suivant réponse a la question)
if [ "$rep_proghalt" = "1" ] ; then
        echo "0 19 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
        else if [ "$rep_proghalt" = "2" ] ; then
                echo "0 21 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
                else if [ "$rep_proghalt" = "3" ] ; then
                        echo "0 23 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
                        else if [ "$rep_proghalt" = "4" ] ; then
                                echo "0 1 * * * root /sbin/shutdown -h now" > /etc/cron.d/prog_extinction
                             fi
                     fi
             fi
fi
# Fin prog



##########################################################################
# Etab Scolaire : Ubuntu, Xubuntu, UbuMate..., Mint (xenial)
##########################################################################
if [ "$rep_profil" = "0" ] ; then 

# Vérification que le système est bien a jour et sans problème de dépendance 
apt-get update ; apt-get -y dist-upgrade ; apt-get -fy install

# Police d'écriture de Microsoft
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt-get -y install ttf-mscorefonts-installer ;

# Oracle Java 8
add-apt-repository -y ppa:webupd8team/java ; apt-get update ; echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections | apt-get -y install oracle-java8-installer

#[ Bureautique ]

# si LO backporté
if [ "$rep_backportlo" = "1" ] ; then 
add-apt-repository -y ppa:libreoffice/ppa ; apt-get update ; apt-get -y upgrade
fi
apt-get -y install libreoffice libreoffice-gtk libreoffice-l10n-fr
apt-get -y install freeplane scribus gnote xournal cups-pdf

#[ Web ]
apt-get -y install firefox chromium-browser flashplugin-downloader pepperflashplugin-nonfree

#[ Video / Audio ]
apt-get -y install imagination openshot audacity vlc x264 x265 ffmpeg2theora flac vorbis-tools lame oggvideotools mplayer ogmrip goobox

#[ Graphisme / Photo ]
apt-get -y install blender sweethome3d gimp pinta inkscape gthumb mypaint hugin shutter

#[ Systeme ]
apt-get -y install gparted vim pyrenamer rar unrar htop diodon p7zip-full 

#[ Mathematiques ]
apt-get -y install geogebra algobox carmetal scilab

#[ Sciences ]
apt-get -y install stellarium avogadro 

#[ Programmation ]
apt-get -y install scratch ghex geany imagemagick idle-python3.5 openjdk-8-jdk


# Concerne Ubuntu / Unity

if [ "$(which unity)" = "/usr/bin/unity" ] ; then  # si Ubuntu/Unity alors :
apt-get -y install ubuntu-restricted-extras ubuntu-restricted-addons unity-tweak-tool nautilus-image-converter nautilus-script-audio-convert
fi

# Concerne Xubuntu / XFCE

if [ "$(which xfwm4)" = "/usr/bin/xfwm4" ] ; then # si Xubuntu/Xfce 16.04 alors :
apt-get -y install xubuntu-restricted-extras xubuntu-restricted-addons xfce4-goodies xfwm4-themes
fi

# Concerne Ubuntu Mate / Mate

if [ "$(which caja)" = "/usr/bin/caja" ] ; then # si Ubuntu Mate 16.04 alors :
apt-get -y install ubuntu-restricted-extras mate-desktop-enivonment-extras
fi

# Concerne Lubuntu / LXDE

if [ "$(which pcmanfm)" = "/usr/bin/pcmanfm" ] ; then  # si Lubuntu / Lxde alors :
apt-get -y install lubuntu-restricted-extras lubuntu-restricted-addons
fi

#nettoyage station 
apt-get -y autoremove --purge ; apt-get -y clean

fi

##########################################################################
# Etab Scolaire : Debian (jessie) 
##########################################################################
if [ "$rep_profil" = "1" ] ; then 

#recup sources.list + maj
wget --no-check-certificate https://raw.githubusercontent.com/sibe39/scripts_divers/master/sources.list_debian-jessie ;
mv -f sources.list_debian-jessie /etc/apt/sources.list ; apt-get update ; apt-get -y --force-yes install pkg-mozilla-archive-keyring deb-multimedia-keyring ;

# Activation multiarch (a désactiver si vous n'avez pas besoin d'appli 32 bits sur la v64bits de Debian)
dpkg --add-architecture i386 ; apt-get update ; 
apt-get -y dist-upgrade ;

# Si environnement de bureau a installer :
if [ "$rep_envbur" = "1" ] ; then
  apt-get -y install xfce4 xfce4-terminal xfce4-goodies xfce4-artwork xfce4-weather-plugin xfce4-whiskermenu-plugin lightdm lightdm-gtk-greeter 
fi

if [ "$rep_envbur" = "2" ] ; then
  apt-get -y install mate-desktop-environment install mate-desktop-environment-extras mate-applets mate-backgrounds mate-themes lightdm lightdm-gtk-greeter
fi

if [ "$rep_envbur" = "3" ] ; then
  apt-get -y install gnome-shell gnome-session gnome-themes gnome-themes-standard gnome-themes-extras gnome-extra-icons 
  apt-get -y install gdm3 gnome-tweak-tool gnome-system-monitor gnome-sudo gnome-shell-extensions gnome-shell-extension-weather 
  apt-get -y install gnome-shell-extension-autohidetopbar gnome-network-admin gnome-screenshot gnome-menus gnome-backgrounds empathy
fi


# Police d'écriture de Microsoft
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt-get -y install ttf-mscorefonts-installer ;

#[ Bureautique ]

# si LO backporté
if [ "$rep_backportlo" = "1" ] ; then 
apt-get -y -t jessie-backports install libreoffice libreoffice-gtk libreoffice-l10n-fr 
else
apt-get -y install libreoffice libreoffice-gtk libreoffice-l10n-fr
fi

apt-get -y install freeplane scribus gnote xournal cups-pdf

#[ Web ]
apt-get -y purge iceweasel iceweasel-l10n-fr ; 
apt-get -y install firefox firefox-l10n-fr chromium chromium-l10n flashplugin-nonfree pepperflashplugin-nonfree

#[ Video / Audio ]
apt-get -y install imagination openshot audacity vlc x264 x265 ffmpeg2theora flac vorbis-tools lame oggvideotools mplayer ogmrip goobox

#lecture dvd
apt-get -y install libdvdread4 libdvdcss ; #libdvdcss dans backport multimedia, sinon utiliser ligne ci-dessous
#wget http://download.videolan.org/pub/debian/testing/libdvdcss2_1.2.13-0_amd64.deb && dpkg -i libdvdcss2_1.2.13-0_amd64.deb 

#[ Graphisme / Photo ]
apt-get -y install blender sweethome3d gimp pinta inkscape gthumb mypaint hugin shutter dia

#[ Systeme ]
apt-get -y install gparted vim pyrenamer rar unrar htop diodon p7zip-full

#[ Mathematiques ]
apt-get -y install geogebra algobox carmetal scilab

#[ Sciences ]
apt-get -y install celestia celestia-common-nonfree stellarium avogadro 

#[ Programmation ]
apt-get -y install scratch ghex geany imagemagick idle-python3.4
apt-get -y -t jessie-backports install openjdk-8

# Google Earth (pour 64 Bits uniquement)
#wget http://extra.linuxmint.com/pool/main/g/google-earth-stable/google-earth-stable_7.1.4.1529-r0_amd64.deb ;
#dpkg -i google-earth-stable_7.1.4.1529-r0_amd64.deb ; apt-get -fy install ;

# Si besoin de Virtualbox en version récente :
#apt-get -y install linux-headers-$(uname -r)
#apt-get -y -t jessie-backports install virtualbox

# Si besoin des pilotes graphiques propriétaires Nvidia :
#apt-get -y install linux-headers-3.16 nvidia-kernel-dkms nvidia-glx mesa-utils
#mkdir /etc/X11/xorg.conf.d ; echo -e 'Section "Device"\n\tIdentifier "My GPU"\n\tDriver "nvidia"\nEndSection' > /etc/X11/xorg.conf.d/20-nvidia.conf

# Pour technologie optimus nvidia/intel sur portable :
#apt-get -y install bumblebee-nvidia primus primus-libs:i386 primus-libs-ia32 ; adduser $USER bumblebee

# Autres logiciels utiles pouvant être backporté sous Jessie (-t jessie-backport install....) :
# blender bluefish ansible bumblebee {kernel linux} emacs firmware-nonfree freshplayerplugin i3-wm inkscape intel-microcode keepassx {driver nvidia} openjdk-8

# Nettoyage 
apt-get -y autoremove --purge ; apt-get clean

fi

##########################################################################
# Technicien Rectorat : Ubuntu, Xubuntu, UbuMate..., Mint (xenial)
##########################################################################
if [ "$rep_profil" = "2" ] ; then 

# Vérification que le système est bien a jour et sans problème de dépendance 
apt-get update ; apt-get -y dist-upgrade ; apt-get -fy install

# Police d'écriture de Microsoft
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt-get -y install ttf-mscorefonts-installer ;

# Oracle Java 8
add-apt-repository -y ppa:webupd8team/java ; apt-get update ; echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections | apt-get -y install oracle-java8-installer

#[ Bureautique ]
# Si LO backport demandé :
if [ "$rep_backportlo" = "1" ] ; then 
add-apt-repository -y ppa:libreoffice/ppa ; apt-get update ; apt-get -y upgrade
fi
apt-get -y install libreoffice libreoffice-gtk libreoffice-l10n-fr

#[ Web ]
apt-get -y install firefox chromium-browser flashplugin-downloader pepperflashplugin-nonfree

#[ Video / Audio ]
apt-get -y install vlc x264 x265

#[ Graphisme / Photo ]
apt-get -y install gimp pinta shutter

#[ Systeme ]
apt-get -y install gparted vim rar unrar htop diodon p7zip-full nmap screen

#[ Programmation ]
apt-get -y install bluefish emacs openjdk-8-jdk

# Assistance pédagogique avec Teamviewer V8 (licence) :
wget http://download.teamviewer.com/download/version_8x/teamviewer_linux.deb ; dpkg -i teamviewer_linux.deb ; apt-get -fy install ;

# Nettoyage 
apt-get -y autoremove --purge ; apt-get clean

fi

##########################################################################
# Technicien Rectorat : Debian (jessie)
##########################################################################
if [ "$rep_profil" = "3" ] ; then 

#recup sources.list + maj
wget --no-check-certificate https://raw.githubusercontent.com/sibe39/scripts_divers/master/sources.list_debian-jessie ;
mv -f sources.list_debian-jessie /etc/apt/sources.list ; apt-get update ; apt-get -y --force-yes install pkg-mozilla-archive-keyring deb-multimedia-keyring ;

# Activation multiarch (a désactiver si vous n'avez pas besoin d'appli 32 bits sur la v64bits de Debian)
dpkg --add-architecture i386 ; apt-get update ; 
apt-get -y dist-upgrade ;

# Si environnement de bureau a installer :
if [ "$rep_envbur" = "1" ] ; then
  apt-get -y install xfce4 xfce4-terminal xfce4-goodies xfce4-artwork xfce4-weather-plugin xfce4-whiskermenu-plugin lightdm lightdm-gtk-greeter 
fi

if [ "$rep_envbur" = "2" ] ; then
  apt-get -y install mate-desktop-environment install mate-desktop-environment-extras mate-applets mate-backgrounds mate-themes lightdm lightdm-gtk-greeter
fi

if [ "$rep_envbur" = "3" ] ; then
  apt-get -y install gnome-shell gnome-session gnome-themes gnome-themes-standard gnome-themes-extras gnome-extra-icons 
  apt-get -y install gdm3 gnome-tweak-tool gnome-system-monitor gnome-sudo gnome-shell-extensions gnome-shell-extension-weather 
  apt-get -y install gnome-shell-extension-autohidetopbar gnome-network-admin gnome-screenshot gnome-menus gnome-backgrounds empathy
fi


# Police d'écriture de Microsoft
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt-get -y install ttf-mscorefonts-installer ;

#[ Bureautique ]

# si LO backporté
if [ "$rep_backportlo" = "1" ] ; then 
apt-get -y -t jessie-backports install libreoffice libreoffice-gtk libreoffice-l10n-fr 
else
apt-get -y install libreoffice libreoffice-gtk libreoffice-l10n-fr
fi

#[ Web ]
apt-get -y install firefox firefox-l10n-fr chromium chromium-l10n midori flashplugin-nonfree pepperflashplugin-nonfree

#[ Video / Audio ]
apt-get -y install vlc x264 x265

#[ Graphisme / Photo ]
apt-get -y install gimp pinta inkscape shutter

#[ Systeme ]
apt-get -y install gparted vim rar unrar htop diodon nmap screen

#[ Dev ]
apt-get -y install bluefish pgadmin3 emacs
apt-get -y -t jessie-backports install openjdk-8

# Virtualbox et en version récente :
apt-get -y install linux-headers-$(uname -r)
apt-get -y -t jessie-backports install virtualbox

# Assistance pédagogique avec Teamviewer V8 (licence) :
wget http://download.teamviewer.com/download/version_8x/teamviewer_linux.deb ; dpkg -i teamviewer_linux.deb ; apt-get -fy install ;

# Si besoin des pilotes graphiques propriétaires Nvidia :
#apt-get -y install linux-headers-3.16 nvidia-kernel-dkms nvidia-glx mesa-utils
#mkdir /etc/X11/xorg.conf.d ; echo -e 'Section "Device"\n\tIdentifier "My GPU"\n\tDriver "nvidia"\nEndSection' > /etc/X11/xorg.conf.d/20-nvidia.conf

# Pour technologie optimus nvidia/intel sur portable :
#apt-get -y install bumblebee-nvidia primus primus-libs:i386 primus-libs-ia32 ; adduser $USER bumblebee

# boot démarrage graphique
apt-get -y install plymouth plymouth-themes ;
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/' /etc/default/grub
update-grub2

# Nettoyage 
apt-get -y autoremove --purge ; apt-get clean


fi

##########################################################################
# Familial : Ubuntu, Xubuntu, UbuMate..., Mint (xenial)
##########################################################################
if [ "$rep_profil" = "4" ] ; then 

# Vérification que le système est bien a jour et sans problème de dépendance 
apt-get update ; apt-get -y dist-upgrade ; apt-get -fy install

# Police d'écriture de Microsoft
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt-get -y install ttf-mscorefonts-installer ;

#[ Bureautique ]
# Si LO backport demandé :
if [ "$rep_backportlo" = "1" ] ; then 
add-apt-repository -y ppa:libreoffice/ppa ; apt-get update ; apt-get -y upgrade
fi
apt-get -y install libreoffice libreoffice-gtk libreoffice-l10n-fr

#[ Web ]
apt-get -y install firefox chromium-browser flashplugin-downloader pepperflashplugin-nonfree thunderbird thunderbird-l10n-fr pidgin skype
apt-get -y install deluge transmission-gtk

#[ Video / Audio ]
apt-get -y install vlc x264 x265 flac lame audacity openshot
# Lire DVD
apt-get -y install libdvdread4 libdvdcss2 


#[ Graphisme / Photo ]
apt-get -y install gimp pinta inkscape shutter

#[ Systeme ]
apt-get -y install gparted vim unrar htop keepassx

#[ Serveur ]
apt-get -y install openssh-server

# Assistance Teamviewer dernière version (11) :
wget http://download.teamviewer.com/download/teamviewer_i386.deb ; dpkg -i teamviewer_i386.deb ; apt-get -fy install ;

# Récupération du script pour switcher HDMI/PC 
wget --no-check-certificate https://raw.githubusercontent.com/sibe39/scripts_divers/master/hdmi_switch.sh ; chmod +x hdmi_switch.sh

# Nettoyage 
apt-get -y autoremove --purge ; apt-get clean


fi

##########################################################################
# Perso1 : Ubuntu, Xubuntu, UbuMate..., Mint (xenial)
##########################################################################
if [ "$rep_profil" = "5" ] ; then 

# Vérification que le système est bien a jour et sans problème de dépendance 
apt-get update ; apt-get -y dist-upgrade ; apt-get -fy install

# Police d'écriture de Microsoft
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt-get -y install ttf-mscorefonts-installer ;

#[ Bureautique ]
# Si LO backport demandé :
if [ "$rep_backportlo" = "1" ] ; then 
add-apt-repository -y ppa:libreoffice/ppa ; apt-get update ; apt-get -y upgrade
fi
apt-get -y install libreoffice libreoffice-gtk libreoffice-l10n-fr libreoffice-templates ;

#[ Web ]
apt-get -y install firefox chromium-browser flashplugin-downloader pepperflashplugin-nonfree thunderbird thunderbird-l10n-fr skype pidgin xchat filezilla

#[ DL ]
apt-get -y install deluge transmission-gtk rtorrent

#[ Gaming ]
apt-get -y install playonlinux steam supertux openarena minetest
# Jeu de stratégie 0ad :
#add-apt-repository -y ppa:wfg/0ad ; apt-get update ; apt-get -y install 0ad
# Jeu FlightGear :
#apt-get -y install flightgear simgear2.4.0 simgear-dev fgfs-base fgfs-aircraft-base fgfs-atlas fgfs-models-base fgfs-scenery-base fgo fgrun

#[ Video / Audio ]
apt-get -y install vlc x264 x265 sound-juicer banshee winff handbrake audacity openshot avidemux

#[ Graphisme / Photo ]
apt-get -y install gimp pinta shutter inskape

#[ Systeme ]
apt-get -y install htop glances gparted vim unrar keepassx hdparm plank virtualbox virtualbox-qt aircrack-ng git build-essential

#[ Programmation ]
apt-get -y install bluefish emacs openjdk-8-jdk
# Android Studio
#apt-add-repository -y ppa:paolorotolo/android-studio ; apt-get update ;  apt-get -y install android-studio

#[ Divers ]


#[ Drivers ]
#apt-get -y install dkms linux-headers-$(uname -r)
#wget --no-check-certificate https://github.com/neurobin/MT7630E/archive/release.zip ; unzip release.zip ; 
#cd MT7630E-release ; make ; make install ;



#[ Serveur ]
apt-get -y install openssh-server

# Assistance Teamviewer dernière version (11) :
wget http://download.teamviewer.com/download/teamviewer_i386.deb ; dpkg -i teamviewer_i386.deb ; apt-get -fy install ;

# Alias
#echo "alias maj='sudo apt update && sudo apt -y upgrade'" >> /home/$LOGIN/.bashrc
#echo "alias majfull='sudo apt update && sudo apt -y full-upgrade'" >> /home/$LOGIN/.bashrc


# Nettoyage 
apt-get -y autoremove --purge ; apt-get clean

fi

##########################################################################
# Perso2 : Debian (jessie)
##########################################################################
if [ "$rep_profil" = "6" ] ; then 

#recup sources.list + maj
wget --no-check-certificate https://raw.githubusercontent.com/sibe39/scripts_divers/master/sources.list_debian-jessie ;
mv -f sources.list_debian-jessie /etc/apt/sources.list ; apt-get update ; 
apt-get -y --force-yes install pkg-mozilla-archive-keyring deb-multimedia-keyring ; apt-get update ; apt-get -y dist-upgrade

# Si environnement de bureau a installer :
if [ "$rep_envbur" = "1" ] ; then
  apt-get -y install xfce4 xfce4-terminal xfce4-goodies xfce4-artwork xfce4-weather-plugin xfce4-whiskermenu-plugin lightdm lightdm-gtk-greeter 
fi

if [ "$rep_envbur" = "2" ] ; then
  apt-get -y install mate-desktop-environment install mate-desktop-environment-extras mate-applets mate-backgrounds mate-themes lightdm lightdm-gtk-greeter
fi

if [ "$rep_envbur" = "3" ] ; then
  apt-get -y install gnome-shell gnome-session gnome-themes gnome-themes-standard gnome-themes-extras gnome-extra-icons 
  apt-get -y install gdm3 gnome-tweak-tool gnome-system-monitor gnome-sudo gnome-shell-extensions gnome-shell-extension-weather 
  apt-get -y install gnome-shell-extension-autohidetopbar gnome-network-admin gnome-screenshot gnome-menus gnome-backgrounds empathy
fi

# Police d'écriture de Microsoft
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | apt-get -y install ttf-mscorefonts-installer ;

#[ Bureautique ]
# si LO backporté
if [ "$rep_backportlo" = "1" ] ; then 
apt-get -y -t jessie-backports install libreoffice libreoffice-gtk libreoffice-l10n-fr libreoffice-templates 
else
apt-get -y install libreoffice libreoffice-gtk libreoffice-l10n-fr libreoffice-templates 
fi

#[ Web ]
apt-get -y install firefox firefox-l10n-fr chromium chromium-l10n flashplugin-nonfree pepperflashplugin-nonfree icedove icedove-l10n-fr pidgin xchat filezilla
#skype
wget http://download.skype.com/linux/skype-debian_4.3.0.37-1_i386.deb ; dpkg -i skype-debian_4.3.0.37-1_i386.deb ; apt-get -fy install ;

#[ DL ]
apt-get -y install deluge transmission-gtk rtorrent

#[ Gaming ]
apt-get -y install playonlinux supertux openarena 
# Jeu de stratégie 0ad + minetest (backporté) :
apt-get -y -t jessie-backports install 0ad minetest

# Jeu FlightGear (a vérifier)
#apt-get -y install flightgear simgear2.4.0 simgear-dev fgfs-base fgfs-aircraft-base fgfs-atlas fgfs-models-base fgfs-scenery-base fgo fgrun

#[ Video / Audio ]
apt-get -y install vlc x264 x265 sound-juicer banshee winff handbrake audacity openshot avidemux

#[ Graphisme / Photo ]
apt-get -y install gimp pinta shutter inskape

#[ Systeme ]
apt-get -y install htop glances gparted vim unrar keepassx hdparm plank aircrack-ng
# Virtualbox a jour 
apt-get -y install linux-headers-$(uname -r)
apt-get -y -t jessie-backports install virtualbox

#[ Programmation ]
apt-get -y install bluefish emacs
apt-get -y -t jessie-backports install openjdk-8

# Android Studio
#wget https://dl.google.com/dl/android/studio/ide-zips/1.5.1.0/android-studio-ide-141.2456560-linux.zip ; unzip android-studio-ide-141.2456560-linux.zip ; mv android-studio-ide-141.2456560-linux ...

#[ Divers ]

# installation plank par compilation (pour xfce ou mate)
#wget https://launchpad.net/plank/1.0/0.11.1/+download/plank-0.11.1.tar.xz ; tar xf plank-0.11.1.tar.xz ; cd plank-0.11.1 ; 
#./configure ; make ; make install ;

#[ Serveur ]
apt-get -y install openssh-server

# Assistance Teamviewer dernière version (11) :
wget http://download.teamviewer.com/download/teamviewer_i386.deb ; dpkg -i teamviewer_i386.deb ; apt-get -fy install ;

# Alias
#echo "alias maj='sudo apt update && sudo apt -y upgrade'" >> /home/$LOGIN/.bashrc
#echo "alias majfull='sudo apt update && sudo apt -y full-upgrade'" >> /home/$LOGIN/.bashrc

# boot démarrage graphique
apt-get -y install plymouth plymouth-themes ;
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/' /etc/default/grub
update-grub2

# Nettoyage 
apt-get -y autoremove --purge ; apt-get clean

fi

##########################################################################
# Perso3 : ArchLinux
##########################################################################
if [ "$rep_profil" = "7" ] ; then 

# Vérification que le système est bien a jour 
pacman --noconfirm -Syu

# Si environnement de bureau a installer :
if [ "$rep_envbur" = "1" ] ; then
  pacman --noconfirm -S xfce4 xfce4-whiskermenu-plugin xfce4-terminal xfce4-artwork xfce4-weather-plugin lightdm lightdm-gtk-greeter 
fi

if [ "$rep_envbur" = "2" ] ; then
  pacman --noconfirm -S mate mate-extra mate-accountsdialog mate-color-manager mate-applets mate-backgrounds mate-themes lightdm lightdm-gtk-greeter
  #mate-menu-advanced & mate-tweak depuis AUR : yaourt -S mate-menu mate-tweak
fi

if [ "$rep_envbur" = "3" ] ; then
  pacman --noconfirm -S gnome-shell gnome-session gnome-themes-standard
  pacman --noconfirm -S gdm gnome-tweak-tool gnome-system-monitor gnome-sudo gnome-shell-extensions gnome-shell-extension-weather 
  pacman --noconfirm -S gnome-shell-extension-autohidetopbar gnome-network-admin gnome-screenshot gnome-menus gnome-backgrounds empathy
fi


# Police d'écriture de Microsoft
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | /usr/bin/debconf-set-selections | pacman --noconfirm -S ttf-mscorefonts-installer ;

#[ Prerequis ]
# ....
pacman --noconfirm -S yaourt

#[ Bureautique ]
pacman --noconfirm -S libreoffice-fresh libreoffice-fresh-fr ;
pacman --noconfirm -S ttf-liberation ttf-dejavu

#[ Web ]
pacman --noconfirm -S firefox firefox-i18n-fr chromium flashplugin thunderbird thunderbird-i18n-fr skype pidgin xchat filezilla

#[ DL ]
pacman --noconfirm -S deluge transmission-gtk rtorrent

#[ Gaming ]
pacman --noconfirm -S minetest 0ad

#[ Video / Audio ]
pacman --noconfirm -S vlc x264 x265 sound-juicer banshee winff handbrake audacity openshot avidemux
#lecture dvd
pacman --noconfirm -S libdvdread libdvdnav libdvdcss

#[ Graphisme / Photo ]
pacman --noconfirm -S gimp pinta shutter inskape

#[ Systeme ]
pacman --noconfirm -S htop glances gparted vim unrar keepassx2 hdparm plank virtualbox virtualbox-qt aircrack-ng wget sudo

#[ Programmation ]
pacman --noconfirm -S bluefish emacs #jdk8-openjdk

#[ Divers ]
# Pour le pilote propriétaire nvidia :
#pacman -S nvidia nvidia-utils nvidia-libgl

# Si besoin de la gestion de la technologie Optimus :
#pacman -S lib32-nvidia-utils bbswitch

# Installation bumblebee avec intel/nvidia (optimus) :
#pacman -S intel-dri xf86-video-intel bumblebee nvidia nvidia-utils lib32-nvidia-utils
#usermod -a -G bumblebee simon
#systemctl enable bumblebeed.service && pacman -S mesa-demos lib32-mesa-demos

#[ Serveur ]
pacman --no-confirm -S openssh

#[ Packaging via AUR ]

# Android Studio
#yaourt -S android-studio

# Teamviewer
#yaourt -S teamviewer

# Pepper-flash pour Chromium
#yaourt -S chromium-pepper-flash 

# Freshplayer pour Firefox 
#yaourt -S freshplayerplugin

# Police d'écriture de Microsoft
#yaourt -S ttf-ms-fonts

fi



##########################################################################
# TonProfil1
##########################################################################
#if [ "$rep_profil" = "8" ] ; then 
# a rajouter ici
#fi

##########################################################################
# TonProfil2
##########################################################################
#if [ "$rep_profil" = "9" ] ; then 
# a rajouter ici
#fi

########################################################################
#FIN
########################################################################
echo "Le script de postinstall a terminé son travail"
read -p "Voulez-vous redémarrer immédiatement ? [O/n] " rep_reboot
if [ "$rep_reboot" = "O" ] || [ "$rep_reboot" = "o" ] || [ "$rep_reboot" = "" ] ; then
  reboot
fi